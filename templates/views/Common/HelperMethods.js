/* @flow */
const constructorName = 'constructor';

/**
 * Binds the passed in reference _this as this to all properties in _this.
 * @param  {object}  _this class to bind this to in methods
 */
export function bindThis(_this: Object) {
  for (const name of Object.getOwnPropertyNames(Object.getPrototypeOf(_this))) {
    if (name !== constructorName) {
      _this[name] = _this[name].bind(_this);
    }
  }
}

/**
 * Lifted straight from material-ui/Autocomplete.
 * Performs a fuzzy text match on a searchText and input
 * @param  {string} searchText The searchText.
 * @param  {string} key        The text to compare to.
 * @return {boolean}           True if a match.
 */
export function fuzzyFilter(searchText: string, key: string): boolean {
  const compareString = key.toLowerCase();
  searchText = searchText.toLowerCase();

  let searchTextIndex = 0;
  for (let index = 0; index < key.length; index++) {
    if (compareString[index] === searchText[searchTextIndex]) {
      searchTextIndex += 1;
    }
  }

  return searchTextIndex === searchText.length;
}

/**
 * Performs exact text search.
 * @param {string} searchText The text to search for.
 * @param {string} text The text to search in.
 */
export function containsString(searchText: string, text: string): boolean {
  return text.indexOf(searchText) > -1;
}

export function addLeadingZero(number: number): string {
  if (number > 9) {
    return number.toString();
  } else {
    return `0${number.toString()}`;
  }
}

function getESTHours(date: Date): number {
  if (date.getDate() === date.getUTCDate()) {
    return date.getUTCHours() - 4;
  } else {
    return 24 - (4 - date.getUTCHours());
  }
}

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export function getMonthFromDate(date: Date): string {
  return getMonthFromIndex(date.getMonth());
}

export function getMonthFromIndex(index: number): string {
  return months[index];
}

export function getStandardESTTimeString(date: Date): string {
  const hour = getESTHours(date);
  return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} at ${addLeadingZero(hour % 12)}:${addLeadingZero(date.getMinutes())}:${addLeadingZero(date.getSeconds())} ${hour < 12 ? 'AM' : 'PM'} EST`;
}

export function getCardinal(num: number): string {
  const j = num % 10;
  const k = num % 100;
  if (j === 1 && k !== 11) {
    return num + 'st';
  }
  if (j === 2 && k !== 12) {
    return num + 'nd';
  }
  if (j === 3 && k !== 13) {
    return num + 'rd';
  }
  return num + 'th';
}
