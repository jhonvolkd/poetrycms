/* @flow */

export type Entry = {
  author: string,
  categories: Array<any>,
  content: {
    brief: string,
    extended: string,
    raw: string,
  },
  publishedDate: any,
  slug: string,
  state: string,
  title: string,
};
