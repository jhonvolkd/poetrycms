/* @flow */
import React from 'react';
import Search from '../Search';

type DefaultProps = {};

type Nav = {
  label: string,
  src: string,
  href: string,
};

type Props = {
  navs: Array<Nav>,
};

type State = {};

export default class Header extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  render(): React$Element<*> {
    return (
      <div className="header">

      </div>
    );
  }
}
