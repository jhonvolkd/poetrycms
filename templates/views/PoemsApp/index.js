/* globals EntriesData, document, window */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Search from './Search';
import Entries from './Entries';
import poemReducers from './Reducers';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

window.onload = () => {
  const store = createStore(poemReducers, {
    entries: EntriesData,
  });

  ReactDOM.render(
    <Provider store={store}>
      <MuiThemeProvider>
        <div>
          <Search />
          <Entries />
        </div>
      </MuiThemeProvider>
    </Provider>,
    document.getElementById('content'),
  );
};
