import {
  SET_UNIFIED_SEARCH_TEXT,
  SET_ENTRIES,
} from '../Actions';


export const unifiedSearchText = (state = '', action) => {
  switch (action.type) {
    case SET_UNIFIED_SEARCH_TEXT:
      return action.text;
    default:
      return state;
  }
};

export const entries = (state = '', action) => {
  switch (action.type) {
    case SET_ENTRIES:
      return action.entries;
    default:
      return state;
  }
};
