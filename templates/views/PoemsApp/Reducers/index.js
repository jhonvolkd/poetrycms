import { combineReducers } from 'redux';
import { unifiedSearchText, entries } from './PoemReducers';

const poemReducers = combineReducers({
  unifiedSearchText,
  entries,
});

export default poemReducers;
