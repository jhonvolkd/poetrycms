import { connect } from 'react-redux';
import EntriesComponent from './component';

const mapStateToProps = (state) => {
  return {
    entries: state.entries,
    unifiedSearchText: state.unifiedSearchText,
  };
};

const Entries = connect(mapStateToProps)(EntriesComponent);
export default Entries;
