/* @flow */
import React from 'react';
import EntryComponent from './Entry';
import { fuzzyFilter, containsString } from '../../Common/HelperMethods';
import { Entry } from '../types';

type DefaultProps = {};

type Props = {
  entries: Array<Entry>,
  unifiedSearchText: string,
};

type State = {
  entries: Array<Entry>,
};

export default class EntriesComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;
  entriesMemoized: Object;

  constructor(props: Props) {
    super(props);
    this.entriesMemoized = {};
    this.state = {
      entries: props.entries,
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.entries !== nextProps.entries) {
      this.entriesMemoized = {};
      this.setState((): Object => {
        return {
          entries: nextProps.entries,
        };
      });
    }
  }

  isSearching(): boolean {
    return typeof this.props.unifiedSearchText === 'string' && this.props.unifiedSearchText.length > 0;
  }

  render(): React$Element<*> {
    const entryComponents = [];
    this.state.entries.forEach((entry: Entry) => {
      if (!this.isSearching() || (fuzzyFilter(this.props.unifiedSearchText, entry.title) || containsString(this.props.unifiedSearchText, entry.content.raw))) {
        if (this.entriesMemoized.hasOwnProperty(entry.slug)) {
          entryComponents.push(this.entriesMemoized[entry.slug]);
        } else {
          const comp = (
            <EntryComponent
              key={entry.title}
              {...entry}
            />
          );
          this.entriesMemoized[entry.slug] = comp;
          entryComponents.push(comp);
        }
      }
    });
    return (
      <div className="entries">
        {entryComponents}
      </div>
    );
  }
}
