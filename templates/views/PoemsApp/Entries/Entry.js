/* @flow */
import React from 'react';
import { Entry } from '../types';
import { EntryPropType } from '../proptypes';
import { bindThis, getMonthFromDate, getCardinal } from '../../Common/HelperMethods';

type DefaultProps = {};

type Props = {

} & Entry;

type State = {

};

export default class EntryComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  constructor(props: Props) {
    super(props);
    bindThis(this);
  }

  displayIfPresent(propName: string, isValid: (val: any) => boolean, className: ?string, displayFunc: (val: any) => string): React$Element<*> {
    if (this.props.hasOwnProperty(propName) && this.props[propName] !== null && isValid(this.props[propName])) {
      return (
        <p className={typeof className === 'string' ? className : ''}>
          {
            typeof displayFunc === 'function'
            ? displayFunc(this.props[propName])
            : this.props[propName]
          }
        </p>
      );
    }
    return '';
  }

  displayPublishedDate(publishedDate: string): string {
    const date = new Date(publishedDate);
    return `${getMonthFromDate(date)} ${getCardinal(date.getDate())}, ${date.getFullYear()}`;
  }
  checkPublishedDateIsValid(publishedDate: string): boolean {
    return publishedDate.length > 0;
  }

  checkAuthorIsValid(author: string): boolean {
    return author.length > 0;
  }

  render(): React$Element<*> {
    return (
      <div className="entry post">
        <h2>
          <a href={`/poems/post/${this.props.slug}`}>{ this.props.title }</a>
        </h2>
        <div className="lead text-muted">
          {this.displayIfPresent('publishedDate', this.checkPublishedDateIsValid, 'published-date', this.displayPublishedDate)}

          {this.displayIfPresent('author', this.checkAuthorIsValid, 'author')}
          <div className="brief" dangerouslySetInnerHTML={{ __html: this.props.content.brief }} />
          {
            this.props.content.extended && this.props.content.extended.length > 0
            ? (
              <p className="read-more">
                <a href={`/poems/post/${this.props.slug}`}>Read more...</a>
              </p>
            )
            : ''
          }
        </div>
       </div>
    );
  }
}

EntryComponent.propTypes = Object.assign({}, EntryPropType);
