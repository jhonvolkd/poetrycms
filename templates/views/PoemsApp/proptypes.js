import PropTypes from 'prop-types';

export const EntryPropType = {
  author: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.any),
  content: PropTypes.shape({
    brief: PropTypes.string,
    extended: PropTypes.string,
  }),
  publishedDate: PropTypes.any,
  slug: PropTypes.string,
  state: PropTypes.string,
  title: PropTypes.string,
};
