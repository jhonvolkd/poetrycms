/* @flow */


/**
 * Creates a standard action creator.
 * @param  {string} type     Value of type field in {@link object} returned by action creator.
 * @param  {...string} argNames Fields in the {@link object}
 * @return {Function}          The action creator.
 */
export function makeActionCreator(type: string, ...argNames: Array<string>): (...args: Array<any>) => Object {
  return function (...args: Array<any>): Object {
    const action = { type };
    argNames.forEach((arg: any, index: any) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
}


export const SET_UNIFIED_SEARCH_TEXT = 'SET_UNIFIED_SEARCH_TEXT';
export const setUnifiedSearchText = makeActionCreator(SET_UNIFIED_SEARCH_TEXT, 'text');

export const SET_ENTRIES = 'SET_ENTRIES';
export const setEntries = makeActionCreator(SET_ENTRIES, 'entries');
