/* @flow */
import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import { bindThis } from '../../Common/HelperMethods';

type DefaultProps = {};

type Props = {
  searchText: string,
  setSearchText: (text: string) => void,
};

type State = {

};

export default class SearchComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  constructor(props: Props) {
    super(props);
    bindThis(this);
  }

  onChange(event: Event) {
    this.props.setSearchText(event.target.value);
  }

  render(): React$Element<*> {
    return (
      <div>
        <Paper zDepth={1}>
          <TextField
            className="search"
            underlineShow={false}
            hintText="Search Poems By Title"
            value={this.props.searchText}
            onChange={this.onChange}
          />
        </Paper>
      </div>
    );
  }
}

SearchComponent.propTypes = {
  searchText: React.PropTypes.string,
  setSearchText: React.PropTypes.func,
};
