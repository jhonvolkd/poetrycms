import { connect } from 'react-redux';
import SearchComponent from './component';
import { setUnifiedSearchText } from '../Actions';

const mapStateToProps = (state) => {
  return {
    searchText: state.unifiedSearchText,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSearchText(text) {
      dispatch(setUnifiedSearchText(text));
    },
  };
};

const Search = connect(mapStateToProps, mapDispatchToProps)(SearchComponent);
export default Search;
