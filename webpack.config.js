const path = require('path');

const merge = require('webpack-merge');
const webpack = require('webpack');

const TARGET = process.env.npm_lifecycle_event;

console.log('TARGET', TARGET);
console.log('dirname', __dirname);
const common = {
  context: path.join(__dirname),
  entry: {
    // define a path for each page
    PoemsApp: ['babel-polyfill', './templates/views/PoemsApp'],
  },
  output: {
    path: path.join(__dirname, './public/js/build'),
    filename: '[name].js',
  },
  module: {
    rules: [
        // CSS
      // {
      //   test: /\.css$/,
      //   include: path.join(__dirname, './app/ReactComponents'),
      //   use: ExtractTextPlugin.extract({
      //     fallback: 'style-loader',
      //     use: 'css-loader'
      //   }),
      // },
        // Transform JavaScript files via Babel
      {
        test: /\.jsx?$/,
        include: path.join(__dirname, './templates/views'),
        exclude: path.join(__dirname, 'node_modules'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
            plugins: ['transform-runtime', 'transform-flow-strip-types'],
          },
        },
      },
    ],
  },
  resolve: {
    // Allow require('./blah') to require blah.jsx
    extensions: ['.js', '.jsx', '.css', '.less'],
    symlinks: false,
  },
};

if (TARGET === 'release') {
  module.exports = merge(common, {
    module: {
      rules: [
        // LESS
      //   {
      //      test: /\.less$/,
      //      include: path.join(__dirname, './app/ReactComponents'),
      //      use: ExtractTextPlugin.extract({
      //          fallback: 'style-loader',
      //          use: [
      //           {
      //               loader: "css-loader" // translates CSS into CommonJS
      //           },
      //           {
      //               loader: "less-loader",
      //               options: {
      //                   paths: [
      //                       path.resolve(ReactCommon),
      //                   ]
      //               } // compiles Less to CSS
      //           }
      //          ],
      //      }),
      //      // currently, ExtractTextPlugin breaks sourcemaps, so it's not useful to have for development
      //      // when launching production, uncomment ExtractTextPlugin and comment the basic style loader
      //      // loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
      //  },
      ],
    },
    plugins: [
      new webpack.EnvironmentPlugin({
        NODE_ENV: 'production',
      }),
    ],
  });
  // console.log('plugins', module.exports.module.plugins);
} else if (TARGET === 'debug-build') {
  module.exports = merge(common, {
    devtool: 'source-map',
    output: {
      sourceMapFilename: '[name].js.map',
    },
    module: {
      rules: [
        // LESS
        {
          test: /\.jsx$|\.js$/,
          loader: 'eslint-loader',
          include: path.join(__dirname, './templates/views'),
          exclude: /bundle\.js$/,
          enforce: 'pre',
          options: {
            quiet: true,
            emitError: false,
            emitWarning: false,
          },
        },
        // {
        //   test: /\.less$/,
        //   include: path.join(__dirname, './app/ReactComponents'),
        //   // use: ['less-loader', 'css-loader', 'style-loader'],
        //   use: [
        //     {
        //       loader: "style-loader" // creates style nodes from JS strings
        //     },
        //     {
        //     loader: "css-loader" // translates CSS into CommonJS
        //     },
        //     {
        //         loader: "less-loader",
        //         options: {
        //             paths: [
        //                 path.resolve(ReactCommon),
        //             ]
        //         } // compiles Less to CSS
        //     }
        //   ],
        //   // currently, ExtractTextPlugin breaks sourcemaps, so it's not useful to have for development
        //   // when launching production, uncomment ExtractTextPlugin and comment the basic style loader
        //   // loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
        // },
      ],
    },
  });
}
